#!/usr/bin/env bash


#############
#	    #	
# Variables #
#	    #
#############

RED='\033[0;31m'
YELLOW='\033[0;93m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

TODAY_DATE=`date +'%d_%m_%Y'`
HOSTNAME=`/usr/bin/hostname`
ANSWER_FILE="/tmp/answer_file.txt"
LOGS="/var/log/spacewalk_setup_${TODAY_DATE}.log"
ID=`/usr/bin/id -u`
RHEL_DISTRO_ID=`awk -F= '/^ID=/{gsub(/"/, "" , $2); print $2}' /etc/os-release`
RHEL_DISTRO_VER_ID=`awk -F= '/^VERSION_ID=/{gsub(/"/, "" , $2); print $2}' /etc/os-release`
VAR_MIN_SIZE=20
DEBUG=""

#################
#	    	#	
# Configuration #
#	    	#
#################

# Make sure your script exit when a command fails
set -o errexit
set -o pipefail

# Exit when your script tries to use undeclared variables
set -o nounset

# Debug mode
DEBUG=${1:-}
[[ "${DEBUG}" ]] && set -o xtrace

#############
#	    #	
# Functions #
#	    #
#############

create_answer_file(){

	echo -e "\n${BLUE}Create Answer File${NC}\n" | tee -a ${LOGS}

	/usr/bin/touch ${ANSWER_FILE}

	cat > ${ANSWER_FILE} << EOF
	admin-email = root@localhost
	ssl-set-cnames = ${HOSTNAME}
	ssl-set-org = Spacewalk Org
	ssl-set-org-unit = spacewalk
	ssl-set-city = Gennevilliers
	ssl-set-state = Ile-de-France
	ssl-set-country = FR
	ssl-password = spacewalk
	ssl-set-email = root@localhost
	ssl-config-sslvhost = Y
	db-backend=postgresql
	db-name=spaceschema
	db-user=spaceuser
	db-password=spacepw
	db-host=localhost
	db-port=5432
	enable-tftp=N
EOF
}

intro(){
	/usr/bin/clear

	/usr/bin/rm -f ${ANSWER_FILE} ${LOGS}

	echo -e "\n${BLUE}You are about to install Spacewalk !${NC}\n" | tee -a ${LOGS}
	sleep 1
	echo -e "${BLUE}Continue ? [y/n]${NC} \c : " | tee -a ${LOGS}

	# Set the no case sensitive mode
	shopt -s nocasematch

	read answer

	case $answer in
		n|no)
		echo -e "\n${BLUE}Exiting installation${NC}\n" | tee -a ${LOGS}
		exit 3
		;;
		
		y|yes)
		;;

		*)
		echo -e "\n${BLUE}Please answer yes/y or no/n ${NC}\n" | tee -a ${LOGS}
		exit 4
		;;
	esac
}

check_prerequisites() {
	echo -e "\nBefore installation let's check whether this system has the prerequisites\n" | tee -a ${LOGS}

	echo -e "Check if the system is RHEL/CentOS 7 : \c" | tee -a ${LOGS}
	if [ "${RHEL_DISTRO_ID}" == "centos" ] && [ "${RHEL_DISTRO_VER_ID}" == "7" ]
	then
		echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
	else
		echo -e "${RED}ko${NC}\n" | tee -a ${LOGS}
		exit 2
	fi

	echo -e "Check if the hostname is a valid FQDN (ex: www.example.com) : \c" | tee -a ${LOGS}
	if [[ `echo "$HOSTNAME" | grep -Pq '(?=^.{1,254}$)(^(?>(?!\d+\.)[a-zA-Z0-9_\-]{1,63}\.?)+(?:[a-zA-Z]{2,})$)'` ]]
	then
		echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
	else
		echo -e "${YELLOW}WARNING${NC}\n" | tee -a ${LOGS}
	fi

	echo -e "Check if another instance of Spacewalk is installed : \c" | tee -a ${LOGS}
	if [[ `/usr/bin/rpm -qa | grep -q spacewalk` ]] || [[ -f /usr/sbin/spacewalk-service ]]
	then
		echo -e "${RED}ko${NC}\n" | tee -a ${LOGS}
	else
		echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
	fi

	echo -e "Check whether the user is root : \c" | tee -a ${LOGS}
	if [ "${ID}" != 0 ]
	then
		echo -e "${RED}ko${NC}\n" | tee -a ${LOGS}
		exit 1
	else
		echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
	fi

	echo -e "Check available space (20G Min) : \c" | tee -a ${LOGS}
	if [[ `/usr/bin/grep -qw "/var" /proc/mounts` ]]
	then
		var_size=`/usr/bin/df -h --output=size / | /usr/bin/awk ' NR==2 ' | /usr/bin/tr -d '[:space:]' | /usr/bin/cut -d"G" -f1`
		if [ ${var_size} -ge ${VAR_MIN_SIZE} ]
		then 
			echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
		else
			echo -e "${RED}ko${NC}\n" | tee -a ${LOGS}
		fi
	else
		var_size=`/usr/bin/df -h --output=size /var | /usr/bin/awk ' NR==2 ' | /usr/bin/tr -d '[:space:]' | /usr/bin/cut -d"G" -f1`
		if [ ${var_size} -ge ${VAR_MIN_SIZE} ]
		then 
			echo -e "${GREEN}OK${NC}\n" | tee -a ${LOGS}
		else
			echo -e "${RED}ko${NC}\n" | tee -a ${LOGS}
		fi
	fi
}


repo_setup() { 
	echo -e "\n${BLUE}Set up the necessary repos (Spacewalk, EPEL,...)${NC}\n" | tee -a ${LOGS}
	sleep 2
	/usr/bin/rpm -Uvh https://copr-be.cloud.fedoraproject.org/results/@spacewalkproject/spacewalk-2.8/epel-7-x86_64/00736372-spacewalk-repo/spacewalk-repo-2.8-11.el7.centos.noarch.rpm | tee -a ${LOGS}
	/usr/bin/rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm | tee -a ${LOGS}
	(cd /etc/yum.repos.d && curl -O https://copr.fedorainfracloud.org/coprs/g/spacewalkproject/java-packages/repo/epel-7/group_spacewalkproject-java-packages-epel-7.repo) | tee -a ${LOGS}
}

spacewalk_setup() {
	echo -e "\n${BLUE}Spacewalk installation${NC}\n" | tee -a ${LOGS}
	sleep 2
	/usr/bin/yum -y install spacewalk-setup-postgresql | tee -a ${LOGS}
	/usr/bin/yum -y install spacewalk-postgresql | tee -a ${LOGS}
	/usr/bin/spacewalk-setup --answer-file=${ANSWER_FILE} | tee -a ${LOGS}
}

firewall_setup(){

 	echo -e "\n${BLUE}Setup the firewall to accept inbound port 80 and 443${NC}\n" | tee -a ${LOGS}
	sleep 2
	/usr/bin/rpm -q --quiet firewalld-*
	if [ "$?" == 0 ]
	then
		/usr/bin/firewall-cmd --add-service=http | tee -a ${LOGS}
		/usr/bin/firewall-cmd --add-service=https | tee -a ${LOGS}
		/usr/bin/firewall-cmd --runtime-to-perm | tee -a ${LOGS}
	else
		/usr/sbin/iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT | tee -a ${LOGS}
		/usr/sbin/iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT | tee -a ${LOGS}
	fi
}


### MAIN ###
intro
check_prerequisites
create_answer_file
repo_setup
firewall_setup
spacewalk_setup
